import 'package:cinemapedia/domain/entities/movie.dart';
import 'package:cinemapedia/insfrastructure/models/tmdb/tmdb_details.dart';
import 'package:cinemapedia/insfrastructure/models/tmdb/tmdb_movie.dart';

class MovieMapper {
  static Movie tmdbtoEnity(TMDBMovie tmdbMovie) => Movie(
      adult: tmdbMovie.adult,
      backdropPath: (tmdbMovie.backdropPath != '')
          ? 'https://image.tmdb.org/t/p/w500/${tmdbMovie.backdropPath}'
          : 'https://clinicadentaldesign.es/wp-content/uploads/2016/10/orionthemes-placeholder-image-750x750.jpg',
      genreIds: tmdbMovie.genreIds.map((e) => e.toString()).toList(),
      id: tmdbMovie.id,
      originalLanguage: tmdbMovie.originalLanguage,
      originalTitle: tmdbMovie.originalTitle,
      overview: tmdbMovie.overview,
      popularity: tmdbMovie.popularity,
      posterPath: (tmdbMovie.posterPath != '')
          ? 'https://image.tmdb.org/t/p/w500/${tmdbMovie.posterPath}'
          : 'https://clinicadentaldesign.es/wp-content/uploads/2016/10/orionthemes-placeholder-image-750x750.jpg',
      releaseDate: tmdbMovie.releaseDate != null
          ? tmdbMovie.releaseDate!
          : DateTime.now(),
      title: tmdbMovie.title,
      video: tmdbMovie.video,
      voteAverage: tmdbMovie.voteAverage,
      voteCount: tmdbMovie.voteCount);

  static Movie movieDetailsToEntity(TmdbDetails tmdbMovie) => Movie(
      adult: tmdbMovie.adult,
      backdropPath: (tmdbMovie.backdropPath != '')
          ? 'https://image.tmdb.org/t/p/w500/${tmdbMovie.backdropPath}'
          : 'https://clinicadentaldesign.es/wp-content/uploads/2016/10/orionthemes-placeholder-image-750x750.jpg',
      genreIds: tmdbMovie.genres.map((e) => e.name).toList(),
      id: tmdbMovie.id,
      originalLanguage: tmdbMovie.originalLanguage,
      originalTitle: tmdbMovie.originalTitle,
      overview: tmdbMovie.overview,
      popularity: tmdbMovie.popularity,
      posterPath: (tmdbMovie.posterPath != '')
          ? 'https://image.tmdb.org/t/p/w500/${tmdbMovie.posterPath}'
          : 'https://clinicadentaldesign.es/wp-content/uploads/2016/10/orionthemes-placeholder-image-750x750.jpg',
      releaseDate: tmdbMovie.releaseDate,
      title: tmdbMovie.title,
      video: tmdbMovie.video,
      voteAverage: tmdbMovie.voteAverage,
      voteCount: tmdbMovie.voteCount);
}
