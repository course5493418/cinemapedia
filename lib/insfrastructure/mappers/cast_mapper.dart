import 'package:cinemapedia/domain/entities/cast.dart';
import 'package:cinemapedia/insfrastructure/models/tmdb/tmdb_credits_reponse.dart';

class CastMapper {
  static Actor castToEntity(Cast cast) => Actor(
      id: cast.id,
      name: cast.name,
      profilePath: cast.profilePath != null
          ? 'https://image.tmdb.org/t/p/w500/${cast.profilePath}'
          : 'https://clinicadentaldesign.es/wp-content/uploads/2016/10/orionthemes-placeholder-image-750x750.jpg',
      character: cast.character);
}
