import 'package:cinemapedia/config/constants/environment.dart';
import 'package:cinemapedia/domain/datasources/cast_datasource.dart';
import 'package:cinemapedia/domain/entities/cast.dart';
import 'package:cinemapedia/insfrastructure/mappers/cast_mapper.dart';
import 'package:cinemapedia/insfrastructure/models/tmdb/tmdb_credits_reponse.dart';
import 'package:dio/dio.dart';

class CastTMDBDatasource extends CastDatasource {
  final dio = Dio(BaseOptions(
      baseUrl: 'https://api.themoviedb.org/3',
      queryParameters: {'api_key': Environment.tmdbKey, 'language': 'es-US'}));

  @override
  Future<List<Actor>> getCastByMovie(String movieId) async {
    final responde = await dio.get('/movie/$movieId/credits');
    final castResponse = CreditsResponse.fromJson(responde.data);
    List<Actor> actors =
        castResponse.cast.map((cast) => CastMapper.castToEntity(cast)).toList();
    return actors;
  }
}
