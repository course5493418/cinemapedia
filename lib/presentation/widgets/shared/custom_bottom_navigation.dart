import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class CustomBottonNaigation extends StatelessWidget {
  final StatefulNavigationShell currentView;
  const CustomBottonNaigation({super.key, required this.currentView});

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
        currentIndex: currentView.currentIndex,
        onTap: (value) => currentView.goBranch(value),
        items: const [
          BottomNavigationBarItem(
              icon: Icon(Icons.home_max), label: 'Home'),
          BottomNavigationBarItem(
              icon: Icon(Icons.label_outline), label: 'Categories'),
          BottomNavigationBarItem(
              icon: Icon(Icons.favorite_outline), label: 'Favorite')
        ]);
  }
}
