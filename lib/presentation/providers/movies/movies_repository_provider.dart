import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:cinemapedia/insfrastructure/datasources/tmdb_movie_datasource.dart';
import 'package:cinemapedia/insfrastructure/repositories/movie_repository_impl.dart';

final movieRepositoryProvider = Provider((ref) {
  return MovieRepositoryImpl(TMDBDataSource());
});
