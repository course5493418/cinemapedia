import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'movies_providers.dart';

final initialLoadingProvider = Provider(
  (ref) {
    final step0 = ref.watch(nowPlayingMoviesProvider).isEmpty;
    final step1 = ref.watch(popularMoviesProvider).isEmpty;
    final step2 = ref.watch(topRatedMoviesProvider).isEmpty;
    final step3 = ref.watch(upcomingMoviesProvider).isEmpty;
    if (step0 || step1 || step2 || step3) return true;
    return false;
  },
);
