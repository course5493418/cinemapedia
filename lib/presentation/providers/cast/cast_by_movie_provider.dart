import 'package:cinemapedia/domain/entities/cast.dart';
import 'package:cinemapedia/presentation/providers/cast/cast_repository_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final castByMovieProvider =
    StateNotifierProvider<CastByMovieNotifier, Map<String, List<Actor>>>((ref) {
  final castRepository = ref.watch(castRepositoryProvider);
  return CastByMovieNotifier(getCast: castRepository.getCastByMovie);
});

typedef GetCastCallback = Future<List<Actor>> Function(String movieId);

class CastByMovieNotifier extends StateNotifier<Map<String, List<Actor>>> {
  final GetCastCallback getCast;
  CastByMovieNotifier({required this.getCast}) : super({});

  Future<void> loadCast(String movieId) async {
    if (state[movieId] != null) return;
    final List<Actor> cast = await getCast(movieId);
    state = {...state, movieId: cast};
  }
}
