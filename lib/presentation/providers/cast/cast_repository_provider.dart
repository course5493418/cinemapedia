import 'package:cinemapedia/insfrastructure/datasources/tmdb_cast_datasource.dart';
import 'package:cinemapedia/insfrastructure/repositories/cast_repository_impl.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final castRepositoryProvider = Provider((ref) {
  return CastRepositoryImpl(CastTMDBDatasource());
});
