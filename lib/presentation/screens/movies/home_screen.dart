import 'package:flutter/material.dart';
import 'package:cinemapedia/presentation/widgets/widgets.dart';
import 'package:go_router/go_router.dart';

class HomeScreen extends StatelessWidget {
  static const String name = 'home_screen';
  final StatefulNavigationShell currentView;
  const HomeScreen({super.key, required this.currentView});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: currentView,
      bottomNavigationBar: CustomBottonNaigation(currentView: currentView),
    );
  }
}
