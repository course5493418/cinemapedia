import 'package:flutter/widgets.dart';
import 'package:go_router/go_router.dart';
import 'package:cinemapedia/presentation/views/views.dart';
import 'package:cinemapedia/presentation/screens/screens.dart';

final GlobalKey<NavigatorState> _rootNavigatorKey =
    GlobalKey<NavigatorState>(debugLabel: 'root');
final GlobalKey<NavigatorState> _tabANavigatorKey =
    GlobalKey<NavigatorState>(debugLabel: 'tabANav');

final appRouter = GoRouter(
  navigatorKey: _rootNavigatorKey,
  initialLocation: '/',
  routes: [
    StatefulShellRoute.indexedStack(
        builder: (_, __, child) {
          return HomeScreen(currentView: child);
        },
        branches: <StatefulShellBranch>[
          StatefulShellBranch(
            navigatorKey: _tabANavigatorKey,
            initialLocation: '/',
            routes: [
              GoRoute(
                  path: '/',
                  builder: (context, state) {
                    return const HomeView();
                  },
                  routes: [
                    GoRoute(
                        path: 'movie/:id',
                        name: MovieScreen.name,
                        parentNavigatorKey: _rootNavigatorKey,
                        builder: (_, state) {
                          final movieId = state.pathParameters['id'] ?? 'no-id';
                          return MovieScreen(movieId: movieId);
                        })
                  ]),
            ],
          ),
          StatefulShellBranch(routes: [
            GoRoute(
              path: '/categories',
              builder: (context, state) {
                return const CategoriesView();
              },
            ),
          ]),
          StatefulShellBranch(routes: [
            GoRoute(
              path: '/favorites',
              builder: (context, state) {
                return const FavoritesView();
              },
            ),
          ]),
        ])

    // GoRoute(
    //     path: '/',
    //     name: HomeScreen.name,
    //     builder: (context, state) => const HomeScreen(
    //           childView: HomeView(),
    //         ),
    //     routes: [
    //       GoRoute(
    //           path: 'movie/:id',
    //           name: MovieScreen.name,
    //           builder: (context, state) {
    //             final movieId = state.pathParameters['id'] ?? 'no-id';
    //             return MovieScreen(movieId: movieId);
    //           })
    //     ])
  ],
);
