import 'package:cinemapedia/domain/entities/cast.dart';

abstract class CastDatasource {
  Future<List<Actor>> getCastByMovie (String movieId);
}