# Cinemapedia

Project of the Flutter course.

App for Movies and TV Show using The Movie DataBase(TMDB) API

| Home                                            | Movie                                           | Search                                          |
|-------------------------------------------------|-------------------------------------------------|-------------------------------------------------|
| ![screenshot1.png](screenshots/screenshot1.png) | ![screenshot3.png](screenshots/screenshot3.png) | ![screenshot2.png](screenshots/screenshot2.png) |